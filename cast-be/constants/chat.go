package constants

const (
	MessageTypeChat    = "chat"
	MessageTypeHistory = "history"
	MessageTypeError   = "error"
)
