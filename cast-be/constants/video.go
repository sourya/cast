package constants

const (
	VideoTypeVOD     = "vod"
	VideoTypeLive    = "live"
	ThumbnailRootDir = "thumbnail"
	ThumbnailDefault = "_default"
	ThumbnailWidth   = 720
	ThumbnailHeight  = 405
)
