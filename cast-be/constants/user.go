package constants

const (
	ProfileRootDir = "profile"
	ProfileDefault = "_default"
	ProfileWidth   = 320
	ProfileHeight  = 320
)
