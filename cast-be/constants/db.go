package constants

const (
	DBCollectionUser = "user"
	DBCollectionVideo = "video"
	DBCollectionLike = "like"
	DBCollectionComment = "comment"
)
