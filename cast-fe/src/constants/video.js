export const VIDEO_TYPE_LIVE = "live";
export const VIDEO_TYPE_VOD = "vod";

export const VIDEO_TITLE_CHAR_LIMIT = 35;
export const VIDEO_TAG_COUNT = 5;
export const VIDEO_TAG_CHAR_LIMIT = 12;
export const VIDEO_DESC_CHAR_LIMIT = 1000;
